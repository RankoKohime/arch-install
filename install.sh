#!/bin/bash

# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
# Get script directory for use in next section
SCRIPTPATH=$(dirname `readlink -f "$0"`)

# Source accessory functions
for i in `find $SCRIPTPATH -name "*.func"`;
  do
    source $i
done
# This is being done in a loop, as sourcing multiple files with globbing appears not to work.



# Determine steps to install:

# Determine if booted in UEFI or BIOS mode
